#!/usr/bin/env bash

## A script to set up a new IMAGE droplet on Digital Ocean,
## (on which we then will set up apertium-fitnesse using provision.sh)
##
## REQUIREMENTS:
## - snapd (sudo apt install snapd)
## - doctl [https://github.com/digitalocean/doctl] (sudo snap install doctl)
## - You have to be authenticated with Digital Ocean.
##   See https://github.com/digitalocean/doctl#authenticating-with-digitalocean
##   for details on how to do so.


source ssh-fingerprint

IMAGE="ubuntu-18-04-x64"

#######################################
printf "Launch a new droplet on DO\n\n"
#######################################


doctl compute droplet create lotophagus --size 1gb --image "$IMAGE" \
      --region fra1 --ssh-keys "$FINGERPRINT" --enable-backups
doctl compute droplet list

# TODO
# - add a new user with sudo rights: adduser <user> sudo && su - <user>
# - generate ssh keys: ssh-keygen -t rsa -b 4096 -C "your_email@example.com" 
# - add ssh key to github account vit Github api: https://stackoverflow.com/questions/16672507/how-to-add-ssh-keys-via-githubs-v3-api#16675966
# - wget https://gitlab.com/selimcan/apertium-fitnesse/raw/master/provision.sh
# - bash provision.sh
