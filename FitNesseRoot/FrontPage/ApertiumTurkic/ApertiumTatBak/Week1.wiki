---
LastModifyingUser: selimcan
Test
---
${CONFIGURATION_WAFERSLIM}

!define TAT_BAK_SRC {${APERTIUM_SRC}apertium-nursery/apertium-tat-bak/}

|import           |
|fixtures.coverage|

!3 14th May 2018 - 18th May 2018

!1 Wikipedia

|Comment:Coverage                                                                                 |
|corpus                                              |transducer                        |coverage?|
|${TURKICCORPORA_SRC}ttwiki-latest-pages-articles.txt|${TAT_BAK_SRC}tat-bak.automorf.bin|>=70     |
|${TURKICCORPORA_SRC}bawiki-latest-pages-articles.txt|${TAT_BAK_SRC}bak-tat.automorf.bin|>=70     |

!1 Tagged corpus

!include .FrontPage.ApertiumTurkic.LePetitPrince.Tatar.Week1

!1 Common vocabulary

!include .FrontPage.ApertiumTurkic.CommonVocabulary.Week1