---
Test
---
${CONFIGURATION_WAFERSLIM}

!3 Imports
|import |
|waferslim.examples.decision_table|
|fixtures.calc.Calc |
|fixtures.translate.Translate|
 
|Calc |
|A|B|multiply?|
|1|2|2 |
|1|0|0 |
|3|5|15 |
|5|5|26 |

|Calc |
|A|B|multiply?|
|1|2|2 |
|1|0|0 |
|3|5|15 |
|5|5|26 |

|Translate |
|A|B|concat?|
|foo|bar|foobar|
|фу|әҗ|фуәҗ|
|фу|әҗ|фувыв|
