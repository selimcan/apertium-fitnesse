
What is it?
===========

Apertium-fitnesse is an unofficial acceptance testing framework/wiki for the
Aperitum project. It is mainly based on Fitnesse_ and Webhook_.

Its main purpose is to give (non-tech-savvy) users an easy way of contributing
their knowledge of natural languages, describe what functionality they want from
a machine translator and its underlying components and thus help Apertiumers
build free/open-source machine translators!

For more information on this type of testing, and on why and how it can be
useful. visit Fitnesse project's website_.

How does it work?
=================

Let's take the Apertium-kaz_ repository on Github as an example.

It is configured_ in such a way that each time a commit is made to its `master'
branch, Github sends a hook to the Webhook_ daemon running on
fitnesse.selimcan.org, and a script (`compile.sh') which does the following gets
triggered on it:

::
    cd apertium-kaz

    git pull

    make

The same happens for other selected Apertium repositories (read: monolingual
packages and translators). The hook that Github sends is a JSON dictionary,
which among other things contains the repository name (i.e. `apertium-kaz'),
which in this case is a clear indicator to which directory to change to and run
the `make' command.

How to run Apertium-fitnesse on your own laptop/server?
=======================================================

Dependencies
------------

`Provision.sh' will set up and run Apertium-fitnesse in a lxc container, thus
the only real dependency is the `lxc' itself. Note that we're talking about the
Canonical's reincarnation of lxc in the `lxd' snap package, which you should
install.

If you're an Apertium developer, you can run an instance of Apertium-fitnesse
on your machine by running provision.sh and get automated tests for the
translators you're working on for free.

Note that provision.sh downloads all of the apertium-* repos, which will take a
lot of bandwith.

If you're interested only in some of the Apertium language modules or
translators, you just might clone the module you're interested in into
either apertium-all/apertium-languages or apertium-all/apertium-incubator/nursery/...
and clone this repo into apertium-all/apertium-tools/, install the dependencies
as described in provision.sh and then launch apertium-fitnesse with
`make launch'.

SampleOutput.png is the output you should expect as of 07.06.2017.
SampleOutput2.png and SampleOutput3.png are more recent examples from 28.02.2018.

An instance of apertium-fitnesse runs on fitnesse.selimcan.org
(running tests is protected by a password).

Running the tests found on fitnesse.selimcan.org locally (for effeciency)
=========================================================================

.. _Fitnesse: http://fitnesse.org
.. _Webhook: https://github.com/adnanh/webhook
.. _Apertium-kaz: https://github.com/apertium/apertium-kaz
.. _configured: https://github.com/apertium/apertium-kaz/settings/hooks
.. _website: http://fitnesse.org
