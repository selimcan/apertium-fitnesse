

from waferslim.converters import TableTableConstants


APERTIUM_TAT_SRC = '../../apertium-languages/apertium-tat'
MODE = 'tat-morph'


class Morph:
    
    def do_table(self, table_rows):
        return tuple([tuple([TableTableConstants.cell_correct(x) for x in row])
                      for row in table_rows])
    
    def apertium(self, inp, srcdir=APERTIUM_TAT_SRC, mode=MODE):
        res = subprocess.run(['apertium', '-d', srcdir, mode],
                             stdout=subprocess.PIPE,
                             input=inp.encode('utf-8'))
        return res.stdout.decode('utf-8')
        
