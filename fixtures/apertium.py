
import subprocess
import re

from utils import apertium

class Apertium(object):

    def __init__(self):
        self.dir = ''
        self.modes = ''
        self.tat = ''
        self.bak = ''

    def set_dir(self, dir):
        self.dir = dir

    def set_modes(self, modes):
        self.modes = modes

    def set_tat(self, tat):
        self.tat = tat

    def set_bak(self, bak):
        self.bak = bak

    def ok(self):
        res = []
        tat_bak = apertium.apertium(self.tat, self.dir, 'tat-bak')
        if self.bak.strip() == tat_bak.strip():
            res.append('tat->bak: OK ')
        else:
            res.append('tat->bak: ' + tat_bak)

        bak_tat = apertium.apertium(self.bak, self.dir, 'bak-tat')
        if self.tat.strip() == bak_tat.strip():
            res.append('bak->tat: OK')
        else:
            res.append('bak->tat: ' + bak_tat)
        return ' '.join(res)
